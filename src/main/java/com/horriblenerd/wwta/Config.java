/*
 * MIT License
 *
 * Copyright (c) 2020 HorribleNerd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.horriblenerd.wwta;

import net.minecraftforge.common.ForgeConfigSpec;

public class Config {
    public static final String CATEGORY_GENERAL = "general";

    public static ForgeConfigSpec CLIENT_CONFIG;

    public static ForgeConfigSpec.BooleanValue SHOW_SECONDS;
    public static ForgeConfigSpec.BooleanValue SHOW_BEFORE;
    public static ForgeConfigSpec.BooleanValue TIME_FORMAT;
    public static ForgeConfigSpec.BooleanValue SHOW_AM_PM;
    public static ForgeConfigSpec.BooleanValue NAME_BASED;
    public static ForgeConfigSpec.IntValue COLOR;

    static {
        ForgeConfigSpec.Builder CLIENT_BUILDER = new ForgeConfigSpec.Builder();

        CLIENT_BUILDER.comment("General settings").push(CATEGORY_GENERAL);
        NAME_BASED = CLIENT_BUILDER.comment("Use a different color for every player").define("name_based", false);
        SHOW_SECONDS = CLIENT_BUILDER.comment("Show seconds").define("show_seconds", true);
        SHOW_BEFORE = CLIENT_BUILDER.comment("Show timestamp before text. (false: show after)").define("show_before", true);
        TIME_FORMAT = CLIENT_BUILDER.comment("24 hour or 12 hour format. (true: 24, false: 12)").define("time_format", true);
        SHOW_AM_PM = CLIENT_BUILDER.comment("Show AM/PM", "Requires TIME_FORMAT to be false").define("show_period", false);
        COLOR = CLIENT_BUILDER.comment("Decimal color code", "https://www.mathsisfun.com/hexadecimal-decimal-colors.html", "Default: 11184810 (gray)").defineInRange("color_code", 11184810, 0, 16777215);

        CLIENT_BUILDER.pop();
        CLIENT_CONFIG = CLIENT_BUILDER.build();
    }

}